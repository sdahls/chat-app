const path = require('path')
const express = require('express');
const app = express();
const socketio=require('socket.io')
const formatMessage = require('./utils/messages')
const { getUsers, userJoin, getCurrentUser, userLeave} = require('./utils/currentUsers')
const fs = require('fs')
const jwt = require('jsonwebtoken')
const cors = require('cors')
app.use(express.static('static'))

const http = require('http');

const server = http.createServer(app)
const io = socketio(server, {
    handlePreflightRequest: (req, res)=>{
        const headers={
            "Access-Control-Allow-Headers": "Content-Type, Authorization",
            "Access-Control-Allow-Origin": req.headers.origin,
            "Access-Control-Allow-Credentials": true
        };
        res.writeHead(200, headers);
        res.end();
    }});

let bot= "Billy the bot"
app.use(express.static(path.join(__dirname, 'static')))
app.use(cors())
app.use(express.json())
const privKey = fs.readFileSync('./keys/private.pem', 'utf-8')
const pubKey = fs.readFileSync('./keys/public.pem', 'utf-8')

let users = [];
io.on('connection', (socket, user) => {
   // console.log('new ws connection...' )
    const token =  socket.handshake.headers.authorization
    jwt.verify(token, privKey, {complete: true}, (error, authData)=>{
        if(error){
           console.log("errer: " + error)
           socket.emit("You have been disconnected for invalidity")
             socket.disconnect();
         
        }else{
         console.log("valid token")
         
        }
    })

    

    socket.on('user', (username)=>{
        io.emit(users)
    })

    socket.emit('message',formatMessage(bot,'Connection established, wellcome to the chat') )  

    //when user connects
    socket.broadcast.emit('message', formatMessage(bot, 'A user has joined the chatt'));

    //client disconnect
    socket.on('disconnect', () => {
        console.log(" user disconnected")
        io.emit('message', formatMessage(bot,'a user has left the chatt'))
    })

    //listen for chatt
    socket.on('chatmessage', (msg, user)=>{
        let msgObj = {"msg":msg, "user": user}
        console.log("Message: " +msg + " " + user)
        io.emit('message', msgObj)
    })

})



app.get('/public', verifyToken,(req,res) =>{
    res.json(pubKey)
})

app.post('/verify', verifyToken, (req,res)=>{
    const token = req.header('authorization')
    console.log(req.header)
   jwt.verify(token, privKey, {complete: true}, (error, authData)=>{
       if(error){
          
       }else{
        
        
       }
   })
    
})


app.post('/createtoken',(req, res)=>{
  
    console.log("toke req: " + req.body.username)
    const user ={
        username: req.body.username,
    }   
    
    jwt.sign(user, privKey, (err, token)=>{
        res.json({token})
        
    })
})

function verifyToken(req, res, next){
    //get auth header value
    const token = req.header('authorization')
    console.log("token: " + token)
    if(token) {
        next()
    }
    else {
        res.sendStatus(403)
    }

}

const PORT = 8080||process.env.PORT;

app.get('/',(req, res)=> res.send('index'))
//{expiresIn: '180s'},


server.listen(PORT, ()=> console.log(`server running on port: ${PORT}`))