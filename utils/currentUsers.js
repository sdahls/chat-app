const currentUsers = []

function userJoin (id, username, token){
    const activeUser = {id, username, tooken}
    
    activeUser.push(currentUsers)

    return activeUser
}


function getCurrentUser(id){
    return currentUsers.find(activeUser=>activeUser.id === id)
}


function userLeave(id){
    const index = currentUsers.findIndex(user => user.id ===id)

    if(index !==-1){
        return currentUsers.splice(index, 1)[0];
    }
}

function getUsers(){
    return currentUsers
}

module.exports ={
    userJoin,
    getCurrentUser,
    userLeave,
    getUsers
}